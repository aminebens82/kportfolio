const frmessages = {
    "home.defaultMessage": "Accueil",
    "home.title": "Introduction",
    "home.description": "title of home page",
    "home.introP1": "Bonjour mon nom est Kolvasna Eng et je suis un programmeur ayant des compétences pour le développement (front-end / back-end). Passionné par la technologie, une de mes plus grandes forces est axé sur l'intégration web côté design.",
    "home.introP2": "Étant un spécialiste TI, je possède plusieurs connaissances et compétences dans diverses sphère du monde informatique.",
    "home.introP3": "Je suis diplômé en Intégration web, Programmation et technologie internet ainsi qu'en Gestion de réseaux et décurité informatique.",
    "home.introP4": "Toujours motivé et inspiré je reste actif dans le monde du design web à titre de pigiste (freelancer), travaillant sur des nouveaux projets web. Pour en s'avoir plus visitez:",
    "home.introP5": "Avec mes talents, découvrez un monde infini de possibilité en matière de design web.",
    "home.introLink1": "Création Web Plus",
    "education.defaultMessage": "Éducation",
    "education.description": "title of home page"
}

export default frmessages