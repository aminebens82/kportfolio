import React, { useEffect } from 'react';
import './App.scss';
import Navigation from './components/Navigation/Navigation';
import { Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import Education from './components/Education';
import {IntlProvider} from 'react-intl';
import enmessages from './translation/en'
import frmessages from './translation/fr'

function loadLocaleData(locale: string) {
  switch (locale) {
    case 'fr':
      return frmessages;
    default:
      return enmessages;
  }
}

const mainMenuItems: any[] = [
  {
    label: 'home',
    path: '/'
  }, 
  {
    label: 'education',
    path: '/education'
  }
]

function App() {
  const [locale, setLocale] = React.useState("en");
  const [messages, setMessages] = React.useState<any>(enmessages);

  const onLocaleChange = ((event: React.ChangeEvent<HTMLSelectElement>) => {
    setLocale(event.currentTarget.value)
  })

  useEffect(() => {
    const loadMessages = () => {
        const messages = loadLocaleData(locale);
        setMessages(messages)
    }
    loadMessages()
  }, [locale])

  return (
    <IntlProvider messages={messages} locale={locale} defaultLocale="en">
      <div className="App">
          <Navigation menuList={mainMenuItems} onLocaleChange={onLocaleChange} />
          <main>
              <Switch>
                <Route exact path="/education">
                    <Education />
                </Route>
                <Route exact path="/">
                    <Home />
                </Route>
              </Switch>
          </main>
      </div>
    </IntlProvider>
  ); 
}

export default App;
