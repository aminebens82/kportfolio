import React from 'react';

function HeaderBanner(props: {src: string, alt: string}) {
    return <img style={{ width: '100%', height: 'auto' }} src={props.src} alt={props.alt} />
}

export default HeaderBanner