import React from 'react';
import { FormattedMessage } from 'react-intl';
import HeaderBanner from './HeaderBanner';

function Education() {
    return (
        <div>
            <HeaderBanner src={'./education.jpg'} alt={'Educayion Banner' }/>
            <FormattedMessage id="education.defaultMessage" />
        </div>
    )
}

export default Education