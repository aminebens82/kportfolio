import React from 'react';
import './Navigation.scss';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

interface Props {
    color?: string
    menuList: any[]
    onLocaleChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
}

const Navigation: React.FC<Props> = ({color, menuList, onLocaleChange}) => {
    return (
        <div className={'Navigation'}>
            <header>
                <nav style={{ color: color ? color : 'white' }} >
                    {menuList.map((menuItem, index) => (
                        <Link key={index} to={menuItem.path}>
                            <FormattedMessage id={`${menuItem.label}.defaultMessage`} />
                        </Link>
                    ))}
                    <div>
                    <select onChange={onLocaleChange}>
                        <option value="en">En</option>
                        <option value="fr">Fr</option>
                    </select>
                    </div>
                </nav>
            </header>
        </div>
    )
}

export default Navigation;
