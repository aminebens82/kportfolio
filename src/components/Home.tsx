import React from 'react';
import { FormattedMessage } from 'react-intl';
import HeaderBanner from './HeaderBanner';
import CenterSection from './CenterSection';

function Home() {
    return (
        <div>
            <HeaderBanner src={'./index_banner.png'} alt={'banner' }/>
            <CenterSection  />
        </div>

    )
}

export default Home
