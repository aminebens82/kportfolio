import React from 'react';
import { FormattedMessage } from 'react-intl';


function CenterSection() {
    return (
        <div>
            <p><FormattedMessage id={'home.title'} /></p>
            <p><FormattedMessage id={'home.introP1'} /></p>
            <p><FormattedMessage id={'home.introP2'} /></p>
            <p><FormattedMessage id={'home.introP3'} /></p>
            <p><FormattedMessage id={'home.introP4'} /></p>
            <p><FormattedMessage id={'home.introP5'} /></p>
        </div>
    )
}

export default CenterSection